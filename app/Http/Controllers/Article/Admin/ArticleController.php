<?php

namespace App\Http\Controllers\Article\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Contracts\View\View;

class ArticleController extends Controller
{
    /**
     * @var ViewFactory
     */
    private $viewFactory;

    /**
     * Create a new controller instance.
     *
     * @param ViewFactory $viewFactory
     */
    public function __construct(
        ViewFactory $viewFactory
    ) {
        $this->middleware('auth:admin');
        $this->viewFactory = $viewFactory;
    }

    /**
     * @return View
     */
    public function getList(): View
    {
        return $this->viewFactory->make('article.admin.grid');
    }

    /**
     * @return View
     */
    public function create(): View
    {
        //@todo
    }
}
